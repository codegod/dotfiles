# set prefix to control-f
set -g prefix C-f

#unbind system defined prefix
unbind C-b

# helps in faster key repetition
set -sg escape-time 0

# start session number from 1 rather than 0
set -g base-index 1

# start pane number from 1 similar to windows
set -g pane-base-index 1

# Make the current window the first window
bind T swap-window -t 1

# source .tmux.conf file
bind r source-file ~/.tmux.conf \; display "Configuration Reloaded!"

# dont mess up with other ctrl-a options by pressing ctrl-a twice
bind C-a send-prefix

# vertical split
bind | split-window -h

# horizontal split
bind - split-window -v

# pane movement similar to vim
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# resize panes
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# to cycle through windows
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+
bind b previous-window

# enable mouse
set -g mouse on

# Toggle mouse on
bind m \
    set -g mouse on \;\
    display 'Mouse: ON'

# Toggle mouse off
bind M \
    set -g mouse off \;\
    display 'Mouse: OFF'

#Note: hold shift key to select the text using mouse

# set 256 color
set -g default-terminal "screen-256color"

# monitor activities in other windows
setw -g monitor-activity on
set -g visual-activity on

# enable vi mode
setw -g mode-keys vi

# remap copy paste keys
unbind [
bind y copy-mode
unbind p
bind p paste-buffer
# bind -t vi-copy 'v' begin-selection
# bind -t vi-copy 'y' copy-selection

# copy to system clipboard
bind C-c run "tmux save-buffer - | xclip -i -sel clipboard"

# paste from system clipboard
bind C-v run "tmux set-buffer \"$(xclip -o -sel clipboard)\"; tmux paste-buffer"

# maximizing and restoring panes
unbind Up
bind Up new-window -d -n tmp \; swap-pane -s tmp.1 \; select-window -t tmp
unbind Down
bind Down last-window \; swap-pane -s tmp.1 \; kill-window -t tmp


# switch between sessions
bind -r ( switch-client -p
bind -r ) switch-client -n

# set zsh as default shell
set -g default-command /bin/zsh
set -g default-shell /bin/zsh

# recording  program output to a log
bind P pipe-pane -o "cat >>~/#W.log" \; display "Toggled logging to ~/#W.log"


# don't allow tmux to rename the window based on commands running
set-window-option -g allow-rename off
#
set -g lock-after-time 300
set -g lock-command "cmatrix -s"
# include status line powerline theme

# status bar theme

set -g status-bg 'colour235'
# set -g message-command-fg 'colour222'
set -g status-justify 'centre'
set -g status-left-length '100'
set -g status 'on'
# set -g pane-active-border-fg 'colour154'
# set -g message-bg 'colour238'
set -g status-right-length '100'
# set -g status-right-attr 'none'
# set -g message-fg 'colour222'
# set -g message-command-bg 'colour238'
# set -g status-attr 'none'
# set -g pane-border-fg 'colour238'

# Theme
# set-option -g pane-border-fg green
# set-option -g pane-active-border-fg green

# set -g status-left-attr 'none'
# setw -g window-status-fg 'colour121'
# setw -g window-status-attr 'none'
# setw -g window-status-activity-bg 'colour235'
# setw -g window-status-activity-attr 'none'
# setw -g window-status-activity-fg 'colour154'
setw -g window-status-separator ''
# setw -g window-status-bg 'colour235'
#set -g status-left '#[fg=colour232,bg=colour154] #S #[fg=colour154,bg=colour238,nobold,nounderscore,noitalics]#[fg=colour222,bg=colour238] #W #[fg=colour238,bg=colour235,nobold,nounderscore,noitalics]#[fg=colour121,bg=colour235] #(whoami)  #(uptime  | cut -d " " -f 1,2,3) #[fg=colour235,bg=colour235,nobold,nounderscore,noitalics]'
#set -g status-right '#[fg=colour235,bg=colour235,nobold,nounderscore,noitalics]#[fg=colour121,bg=colour235] %r  %a  %Y #[fg=colour238,bg=colour235,nobold,nounderscore,noitalics]#[fg=colour222,bg=colour238] #H #[fg=colour154,bg=colour238,nobold,nounderscore,noitalics]#[fg=colour232,bg=colour154] #(rainbarf --battery --remaining --no-rgb) '
set-option -g status-left "#(~/.config/tmux/tmux-powerline/powerline.sh left)"
set-option -g status-right "#(~/.config/tmux/tmux-powerline/powerline.sh right)"

setw -g window-status-format '#[fg=colour235,bg=colour235,nobold,nounderscore,noitalics]#[default] #I  #W #[fg=colour235,bg=colour235,nobold,nounderscore,noitalics]'
setw -g window-status-current-format '#[fg=colour235,bg=colour238,nobold,nounderscore,noitalics]#[fg=colour222,bg=colour238] #I  #W  #F #[fg=colour238,bg=colour235,nobold,nounderscore,noitalics]'

