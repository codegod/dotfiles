# Dot files are here

## Fonts

- DejaVu Sans Mono for Powerline Book
- ttf-iosevka
- nerd-fonts-complete-mono-glyphs

## Requirements

- Arch
  - cinammon
  - Guake terminal
  - Albert
  - plank
  - Theme
    - Window border: Flat remix gtk darkest solid noborder
    - Icons: Flat-remix blue dark
    - Controls: Flat remix gtk darkest solid noborder
    - Desktop: Flat remix gtk darkest solid noborder
  - tmux
  - antigen
  - robo-3t
  - valentina-studio
  - postman-bin
  - gitkraken
  - powerline
  - powerline-fonts
  - powerlevel10k
